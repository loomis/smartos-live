var imgadm = require('./lib/imgadm');
var bunyan = require('bunyan');
var log = bunyan.createLogger({name: 'foo', level: 'trace'});
imgadm.createTool({log: log}, function (err, tool) {

    //tool.listImages(function (err, images) { 
    //    console.log(images)
    //});

/**
 * Get import info on the given image/repo from sources.
 *
 * @param opts {Object}
 *      - @param arg {String} Required. The import arg, e.g. a UUID for an
 *        IMGAPI source or a `docker pull ARG` for a Docker source.
 *      - @param sources {Array} Optional. An optional override to the set
 *        of sources to search. Defaults to `self.sources`.
 *      - @param ensureActive {Boolean} Optional. Default true. Set to false
 *        to have imgapi source searches exclude inactive images.
 * @param cb {Function} `function (err, importInfo)` where `importInfo`
 *      is `{uuid: <uuid>, source: <source>, ...opt source-specific fields...}`
 */
    var addOpts = {
        url: 'https://docker.io',
        type: 'docker',
        insecure: true
    };
    tool.configAddSource(addOpts, true,
       function (err, changed, source) {
            if (err) {
                console.log(err);
            } else if (changed) {
                console.log('Added %s', source);
            } else {
                console.log('Already have %s, no change', source);
            }
        }
    );

    var opts = {
        arg: "jamesloomis/android-build",
    }
    tool.sourcesGetImportInfo(opts, function(err, importInfo) {
        if(err) {
            console.log("error")
            console.log(err)
        } else {
            console.log("importInfo")
            console.log(importInfo)
        }
    })
       
});
